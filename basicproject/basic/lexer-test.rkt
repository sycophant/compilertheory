#lang br
(require "lexer.rkt" brag/support rackunit)

(define (lex str)
  (apply-port-proc basic-lexer str))

(check-equal? (lex "") empty)
(check-equal?
 (lex " ")
 (list (srcloc-token (token " " #:skip? #t)
                     (srcloc 'string 1 0 1 1))))
(check-equal?
 (lex "REM IGNORED\n")
 (list (srcloc-token (token 'REM "REM IGNORED")
                     (srcloc 'string 1 0 1 11))
       (srcloc-token (token 'NEWLINE "\n")
                     (srcloc 'string 1 11 12 1))))
(check-equal?
 (lex "PRINT")
 (list (srcloc-token (token "PRINT" "PRINT")
                     (srcloc 'string 1 0 1 5))))
(check-equal?
 (lex "GOTO")
 (list (srcloc-token (token "GOTO" "GOTO")
                     (srcloc 'string 1 0 1 4))))
(check-equal?
 (lex "END")
 (list (srcloc-token (token "END" "END")
                     (srcloc 'string 1 0 1 3))))
(check-equal?
 (lex "+")
 (list (srcloc-token (token "+" "+")
                     (srcloc 'string 1 0 1 1))))
(check-equal?
 (lex ";")
 (list (srcloc-token (token ";" ";")
                     (srcloc 'string 1 0 1 1))))
(check-equal?
 (lex ":")
 (list (srcloc-token (token ":" ":")
                     (srcloc 'string 1 0 1 1))))
(check-equal?
 (lex "12")
 (list (srcloc-token (token 'INTEGER 12)
                     (srcloc 'string 1 0 1 2))))
(check-equal?
 (lex "1.2")
 (list (srcloc-token (token 'DECIMAL 1.2)
                     (srcloc 'string 1 0 1 3))))
(check-equal?
 (lex "12.")
 (list (srcloc-token (token 'DECIMAL 12.)
                     (srcloc 'string 1 0 1 3))))
(check-equal?
 (lex ".12")
 (list (srcloc-token (token 'DECIMAL .12)
                     (srcloc 'string 1 0 1 3))))
(check-equal?
 (lex "\"foo\"")
 (list (srcloc-token (token 'STRING "foo")
                     (srcloc 'string 1 0 1 5))))
(check-equal?
 (lex "'FOO'")
 (list (srcloc-token (token 'STRING "FOO")
                     (srcloc 'string 1 0 1 5))))
(check-exn exn:fail:read? (lambda () (lex "X")))